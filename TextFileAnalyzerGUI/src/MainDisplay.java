import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;
import javax.swing.*;
import javax.swing.filechooser.*;

 public class MainDisplay extends JFrame{
 
    private String text;
    private String originalText;
    private static JTextArea fData;
    private static JTextArea fHistory;
    private boolean Symbols = true;
    private FileWriter fw;
    private String fileName;
 
    public static void main(String[] args) {
 
        //Frame definition
        //Also includes JPanel which presents the divide the content of the GUI.
        JPanel blank = new JPanel();
        JFrame mFrame = new JFrame("Text File Analyzer");
        mFrame.getContentPane().setBackground(Color.BLACK);
        final JPanel titlePanel = new JPanel(new GridLayout());
        titlePanel.setBackground(Color.BLACK);
        final JPanel textSection = new JPanel(new GridLayout());
        final JPanel buttonSection = new JPanel(new GridLayout());
        mFrame.setSize(new Dimension(700,500));
        mFrame.setLocation(100,100);
 
        JLabel reportT = new JLabel("Text File Report",JLabel.CENTER);
        reportT.setFont(new Font("Copperplate Gothic Bold", Font.BOLD,12));//!
        reportT.setForeground(Color.white);
 
        reportT.setBackground(Color.BLACK);
    
        reportT.setOpaque(true);
        fData = new JTextArea();
        fData.setBackground(Color.BLACK); //!
        fData.setForeground(Color.WHITE);
        fData.setEditable(false);
        JScrollPane fDataHolder = new JScrollPane(fData);
        fHistory = new JTextArea(); // needs to be placed below.
        fHistory.setBackground(Color.BLACK);
        fHistory.setForeground(Color.WHITE);
        fHistory.setFont(new Font("monospaced", Font.PLAIN,12 ));
        fHistory.setEditable(false);
        JScrollPane fHistoryHolder = new JScrollPane(fHistory);
        JSplitPane split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,fDataHolder,fHistoryHolder);
        split.setResizeWeight(0.5);
        split.setDividerLocation(0.8);
        titlePanel.add(reportT);
        //TextBoxes are added to the JPanel called TextSection
        textSection.add(split);
        textSection.setVisible(true);

 
 
        JButton uploadB = new JButton("UPLOAD FILE");
 
        //Assign upload button actions
        MainDisplay internal = new MainDisplay();
        uploadB.addActionListener(internal.new loadFile());
 
        JButton helpB = new JButton("HELP");
        helpB.addActionListener(internal.new helpWindow());
        
        JButton toggleSymbolsB = new JButton("Toggle Symbols");
        toggleSymbolsB.addActionListener(internal.new togglePressed());
        
      
            
        
        helpB.setBackground(Color.RED);
        helpB.setOpaque(true);
        uploadB.setBackground(Color.GREEN);
        uploadB.setOpaque(true);
 
        //Upload button and Help buttons are added to the ButtonSection JPanel.
        buttonSection.add(uploadB);
        buttonSection.add(toggleSymbolsB);
        buttonSection.add(blank);
        buttonSection.add(helpB);
        buttonSection.setVisible(true);
    
 
        //JPanels are added to the main frame(mFrame).
        mFrame.add(titlePanel, BorderLayout.NORTH);
        mFrame.add(textSection, BorderLayout.CENTER);
        mFrame.add(buttonSection, BorderLayout.SOUTH);
 
        //frame is now visible.
        mFrame.setVisible(true);
        
        fData.setText("<This is default text>");  
        
    }
    
    
    //Send the string to the text variable that the analysis will use
    private void setTextVar(String input)
    {
        text = input;
    }
    //Return the current text variable
    private String getTextVar()
    {
        return text;        
    }
    
    class togglePressed implements ActionListener
    {
        //Gets triggered when the button is clicked because an actionevent occurs
        public void actionPerformed(ActionEvent arg0)
        {
            System.out.println(originalText);
            if (Symbols)
            {
                setTextVar(originalText);
                String returnString = "";
                Pattern regex = Pattern.compile("([0-9a-zA-Z \n]+)");
                Matcher matcher = regex.matcher(getTextVar());

                while (matcher.find()) {
                    returnString = returnString + matcher.group();
                }
                setTextVar(returnString);
                try 
                {
                    fileInfo(getTextVar());
                } 
                catch (IOException e) 
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                Symbols = false;
            }
            else
            {
                setTextVar(originalText);
                try 
                {
                    fileInfo(getTextVar());
                } 
                catch (IOException e) 
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                Symbols = true;
            }
        }
    }
    private void appendLog(String fileName)
    {
        File file = new File("FileLogs.txt");
        // If file doesn't exists, then create it
        try 
        {
            if (!file.exists()) 
            {
                file.createNewFile();
            }
            fw = new FileWriter(file, true);
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            Date date = new Date();
            fw.append(fileName + "#" + dateFormat.format(date) + "#" + countLines(text) + "#" + 
                      countBlankLines(text) + "#" + countSpaces(text) + "#" +
                      averageCharPerLine(text) + "#" + averageWordLength(text) + "#\n");
            //Filename 0
            //Date 1
            //CountLines 2
            //countBlankLines 3
            //countSpaces 4
            //averageCharPerLine 5
            //averageWordLength 6
            fw.close();
            if (file.exists()) 
            {
                System.out.println("File written at " + file.getAbsolutePath());
            }
            else
            {
                System.out.println("File not written.");
            }
        } 
        catch (IOException e) 
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    //Get log information of processed files
    private String readLog()
    {
        //So I made this way more complex than it needed to be
        //Changed it to be simple.
        String outputString = "";
        try
        {   
            File file = new File("FileLogs.txt");
            //Use standard scanner class to get data from the file
            Scanner tempScan = new Scanner(file);
            while (tempScan.hasNextLine())
            {
                outputString = outputString + "\n" + tempScan.nextLine();
            }
        }
        catch (Exception e)
        {
            //Print any exceptions to debug
            e.printStackTrace();
        }
        return outputString;
    }
        
    //Prompt user for file upload and read in contents
    private File getFile()
    {
        //Instantiate the file chooser dialog
        JFileChooser jfchoose = new JFileChooser();
        jfchoose.setDialogTitle("Choose text file to load");
        jfchoose.setFileSelectionMode(JFileChooser.FILES_ONLY);
        //Set file chooser to default to text files
        FileNameExtensionFilter txtFilter = new FileNameExtensionFilter("txt files","txt");
        jfchoose.setFileFilter(txtFilter);
 
        //If someone closes the file open dialog or hits cancel, handle that as null
        if (jfchoose.showOpenDialog(null) != JFileChooser.APPROVE_OPTION)
        {
            return null;
        }
        //the file object (not the data contained in the file)
        fileName =  jfchoose.getSelectedFile().getName();
        return jfchoose.getSelectedFile();
    }
    class loadFile implements ActionListener
    {
        //Gets triggered when the button is clicked because an actionevent occurs
        public void actionPerformed(ActionEvent arg0)
        {
            //Clear text variable
            setTextVar("");
            //This is a try/catch because there's the race condition of loading a file and having it become deleted before the file gets fulled uploaded.
            try
            {
                //Use standard scanner class to get data from the file
                Scanner tempScan = new Scanner(getFile());
                try
                {
                    while (tempScan.hasNextLine())
                    {
                        //Force line separation for easier parsing, also the newline will be lost when using Scanner class
                        setTextVar(getTextVar() + tempScan.nextLine() + '\n');
                    }
                }
                //No matter what happens, finally block executes
                finally
                {
                    //Close file to free memory
                    tempScan.close();
                    originalText = getTextVar();
                }
             }
             catch (FileNotFoundException e)
             {
                //Happens on cancel/close, just logging
                e.printStackTrace();
             }
             //When this is done, the text variable should have the file text line-delimited

            try
            {
                fileInfo(getTextVar());
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }
        /*method for trying to count lines.  Uses the file text.  I am unsure if this will
         * work because I don't know if it registers the load file class
         */
     public int countLines(String text) throws IOException
                     {        // number of lines
                         int lineNumber = 0;
                            // separates lines into indexes in string array
                         String[] lines = text.split("\n");
                            
                         for(int i = 0;i<lines.length;i++)
                        {
                            lineNumber++;
                         }
                             //returns the line number
                            return lineNumber;
             }
            // method to count the number of blank lines. Uses the String as a parameter to parse
            public int countBlankLines(String text) throws IOException
            {        // number of blank lines
                 int blankLineNumber = 0;
                    // separates lines into indexes in string array
                 String[] lines = text.split("\n");
                    
                 for(int i = 0;i<lines.length;i++)
                 {
                    //replaced if statement because it was wrong if there was only a \n in the line
                    if (lines[i].trim().equals(""))
                    //if (lines[i] == "") 
                    {
                        blankLineNumber++;
                    }
                 }
                     //returns the blank line number
                     return blankLineNumber;
            }
            //this method counts number of words in the file
            public int countOfWords(String text) throws IOException
            {
                int numWords = 0;
                
                String[] lines = text.split("\n");
                
                for(int i = 0;i<lines.length;i++)
                {   // parse line of text into words
                    String words[] = lines[i].split(" ");
                    // traverse through to each word
                    for (int j = 0;j<words.length;j++)
                    {       
                     // increment number of words
                        numWords++;
                    }
                    
                }
                return numWords;
            }
            // method to count number of spaces from a String parameter
            public int countSpaces(String text) throws IOException
            {       // number of space
                int spaceNumber = 0;
                
                    // separates lines into indexes in string array
               String[] lines = text.split("\n");
                    
                 for(int i = 0;i<lines.length;i++)
                 {
                    for(int j = 0;j<lines[i].length();j++)
                    {
                        if(lines[i].charAt(j) == ' ')
                            spaceNumber++;
                    }
                 }
                    // return number of spaces
                return spaceNumber;
            }
            // method to calculate the average characters per line using a String parameter. Uses the countline() method 
            public int averageCharPerLine(String text) throws IOException
        {
                                
               
                
                    // Average char per line
                int countAvgChar = 0;
                    // line count
                int lc = countLines(text);
                    // separates lines into indexes in string array
                String[] lines = text.split("\n");
                    
                 for(int i = 0;i<lines.length;i++)
                 {
                    countAvgChar = countAvgChar + lines[i].length();
                 }
                    // calculation of the average
                countAvgChar=countAvgChar/lc;
                
                return countAvgChar;
            }
            // method to count average word length 
            public int averageWordLength(String text) throws IOException
            { int charsAverage = averageCharPerLine(text);
                int words = countOfWords (text);
                int charsLines = countLines(text);
                int chars = charsAverage * charsLines;
            
                int total = chars/words;
                return total;
                /*
                // the method that I made two or three weeks ago doesn't seem to have any bugs   
            // Average Word Length
                int avgWordLength = 0;
                    // number of words
            int numOfWords = 0;
                    // separates lines into indexes in string array
                 String[] lines = text.split("\n");
                    
                 for(int i = 0;i<lines.length;i++)
                 {      // parse line of text into words
                    String words[] = lines[i].split(" ");
                        // 
                    numOfWords = words.length;
                        // traverse through to each word
                    for (int j = 0;j<words.length;j++)
                    {       // add to sum of avg words
                        avgWordLength = avgWordLength + words[j].length();
                         // increment number of words
                        numOfWords++;
                    }
                    
                 }  // return calculation of avg word length
                return avgWordLength/numOfWords;
                */
            }
            // method to display the info of the text in a string using a String parameter
            public void fileInfo(String text) throws IOException
            {       
                String info="";
                    //show formatted info of text by using the other methods
                info=   "Lines: " + countLines(text) +
                        "\nBlank Lines: " + countBlankLines(text) +
                    "\nSpaces: " + countSpaces(text) +
                        "\nAverage Characters Per Line: " + averageCharPerLine(text) +
                        "\nAverage Word Length: " + averageWordLength(text) +
                        "\nMost Common Words:\n" + mostCommonWords(text) +
                        "\n\n---------------------------\nFile Contents below:" + 
                        "\n---------------------------\n" + getTextVar();
                //Moved append log over here to make more sense of it
                appendLog(fileName);
                fData.setText(info);
                fHistory.setText(fileHistory());
            }
            
            
              //method to display files history
            public String fileHistory() throws IOException
            {
                String history;
                history = readLog();
                String outputString = ( String.format("%19s", "FILENAME")+
                                        String.format("%19s", "DATE")+
                                        String.format("%19s", "COUNT LINES")+
                                        String.format("%19s", "BLANK LINES")+
                                        String.format("%19s", "SPACES")+
                                        String.format("%19s", "AVG CHARS/LINE")+
                                        String.format("%19s", "AVG WORD LENGTH")) + 
                							String.format("%19s", " avg count Lines") +
                							String.format("%19s", " avg blank lines")+
                							String.format("%19s", " avg spaces")+
                							String.format("%19s", " average char per line")+
                							String.format("%19s", " average word length")+
                							"\n";
                int totalCountLines = 0;
                int avgCountLines = 0;
                	int totalBlankLines = 0;
                	int avgBlankLines = 0;
                	int totalSpaces = 0;
                	int avgSpaces = 0;
                	int totalAvgChars = 0;
                	int avgAvgChars = 0;
                	int totalAvgWord = 0;
                	int avgAvgWord = 0;
               
                int y = 0;

                for (int x = 1; x < history.split("\n").length; x++)
                {
                		y ++ ;
                    String[] line = history.split("\n");
                    String FILE = line[x].split("#")[0];
                    String DATE = line[x].split("#")[1];
                    String COUNTLINES = line[x].split("#")[2];
                    String BLANKLINES = line[x].split("#")[3];
                    String SPACES = line[x].split("#")[4];
                    String AVGCHARS = line[x].split("#")[5];
                    String AVGWORD = line[x].split("#")[6];
                    
                    totalCountLines =  ((totalCountLines + Integer.parseInt(COUNTLINES)));
                    avgCountLines = (totalCountLines / y);
                    
                    totalBlankLines =  ((totalBlankLines + Integer.parseInt(BLANKLINES)));
                    avgBlankLines = (totalBlankLines / y);
                    
                    totalSpaces = ((totalSpaces + Integer.parseInt(SPACES)));
                    avgSpaces = (totalSpaces / y);
                    
                    totalAvgChars = ((totalAvgChars + Integer.parseInt(AVGCHARS)));
                    avgAvgChars = (totalAvgChars/ y);
                    
                    totalAvgWord = ((totalAvgWord + Integer.parseInt(AVGWORD)));
                    avgAvgWord = (totalAvgWord /y);
                    
                    
                    outputString = outputString + ( String.format("%19s", FILE)+
                                                    String.format("%19s", DATE)+
                                                    String.format("%19s", COUNTLINES)+
                                                    String.format("%19s", BLANKLINES)+
                                                    String.format("%19s", SPACES)+
                                                    String.format("%19s", AVGCHARS)+
                                                    String.format("%19s", AVGWORD)+
                                                    String.format("%19s", avgCountLines)+
                                                    String.format("%19s", avgBlankLines)+
                                                    String.format("%19s", avgSpaces)+
                                                    String.format("%19s", avgAvgChars)+
                                                    String.format("%19s", avgAvgWord)+
                            "\n" );
                    
                    
                }
                System.out.println("OUTPUT:\n" + avgCountLines + outputString );
                return outputString;
            }
            
            // method to find the most common words in the text, returning them in a an array of strings
            public String mostCommonWords(String text) throws IOException 
            {
                int freq = 0, max = 0;
                String word = null;
                ArrayList<String> wordList = new ArrayList<>();
                
                // separates lines into indexes in string array
                String[] lines = text.split("\n");
                
                // parse text into separate words
                for(String item: lines)
                {      // parse line of text into words
                    String[] words = item.split(" ");
                    for(String x: words)
                    {  
                        if(!x.trim().equals(""))
                        {
                            wordList.add(x);// add to mcw list
                        }
                    }
                }
                
                // find the maximum number of occurrences of the words in list
                for (Iterator<String> iter = wordList.iterator(); iter.hasNext(); )
                {
                    word = iter.next();
                    freq = Collections.frequency(wordList, word);
                    if(freq > max)
                    {
                        max = freq;
                    }
                }
                // remove any words that do not have the most occurrences
                for(Iterator<String> iter = wordList.iterator(); iter.hasNext(); )
                {
                    word = iter.next();
                    freq = Collections.frequency(wordList,  word);
                    if(freq != max)
                    {
                        iter.remove();
                    }
                }
                // remove duplicates because hashSet does not accept them
                Set<String> hashSet = new HashSet<>();
                hashSet.addAll(wordList);
                wordList.clear();
                wordList.addAll(hashSet);
                // return List with most common words
                return wordList.toString();
            }
           
       // ActionListener for the Help button
     class helpWindow implements ActionListener{ 
            
         public void actionPerformed(ActionEvent e) {
            JFrame hFrame = new JFrame("Help");
             hFrame.getContentPane().setBackground(Color.BLACK);
        
            
             hFrame.setSize(400, 400);
            hFrame.setLayout(new BoxLayout(hFrame.getContentPane(), BoxLayout.Y_AXIS));
             
             
             final JPanel titlePanel = new JPanel();
             final JPanel helpPanel1 = new JPanel();
             titlePanel.setBackground(Color.BLACK);
             helpPanel1.setBackground(Color.BLACK);
             JLabel infoT = new JLabel("How To Use Tutorial");
             JLabel info = new JLabel("How To Use Tutorial");
             JLabel tInfo = new JLabel("Information about The Toggle Feature");
             info.setText("<html>" + "1)Click on the UPLOAD FILE button" + "<br>" + "2)select a file(.txt)" + "<br>" + "3) Lastly, click upload." + " </htmt>");
             tInfo.setText("<html>"+"Toggle Symbols button:This features allows you to see content of the file without punctuation"+"<br>"+"1)Click on Toggle Symbols button to remove punctuation "+"</html>");
             tInfo.setForeground(Color.WHITE);
             info.setForeground(Color.WHITE);
             infoT.setForeground(Color.WHITE);
             infoT.setFont(new Font("Copperplate Gothic Bold", Font.BOLD,12));
             info.setFont(new Font("Copperplate Gothic Bold", Font.BOLD,12));
             tInfo.setFont(new Font("Copperplate Gothic Bold", Font.BOLD,12));
            
             helpPanel1.add(info);
             helpPanel1.add(infoT);
             helpPanel1.add(tInfo);
             
             hFrame.getContentPane().add(infoT);
             hFrame.getContentPane().add(helpPanel1);
             hFrame.getContentPane().add(tInfo);
             hFrame.setVisible(true);
        
            }
        }
    }
   



